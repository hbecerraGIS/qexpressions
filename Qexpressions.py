#Dissolve

BUFFER(COLLECT($geometry),0)

#Dividir campo por caracter y posición

string_to_array("Nombre", ' ')[2]

#Calcular un campo basado en la capa que intersecta

array_to_string(
 aggregate( 
  layer:='capa_con_atributos',
  aggregate:='array_agg',
  expression:="campo",
  filter:=intersects(($geometry),geometry(@parent))
 )
)

#Versión corta

aggregate
('capa',
'concatenate',
"campo",
intersects($geometry,geometry(@parent)))

#Crear líneas perpendiculares sobre un punto simple

make_line($geometry, translate($geometry, 10, 10))

make_line($geometry, translate($geometry, 100* cos(radians("angle")), 100* sin(radians(-"angle"))))
  
make_line($geometry, translate($geometry, -100* cos(radians("angle")), -100* sin(radians(-"angle"))))

make_line($geometry, translate($geometry, 100* cos(radians("angle")), 100* sin(radians(-"angle"))),translate($geometry, -100* cos(radians("angle")), -100* sin(radians(-"angle"))))

#qilometraje

lpad(string_to_array("distance"/1000,'.')[0],3,0)||'+'||lpad(right("distance",3),3,0)

#LRS

#Suavizar contornos

smooth($geometry, 10, 0.5, 0.1)

#Contar entidades de una capa

to_string(aggregate('capa','count',''))


lpad(string_to_array(round("measure",0)/1000,'.')[0],3,0)||'+'||lpad(right(round("measure",0),3),3,0)
lpad(string_to_array(round("measure",0)/1000,'.')[0],3,0)||'+'||lpad(left(string_to_array("measure"/1000,'.')[1],3),3,0)

#Formato de fecha y hora

title(format_date(to_datetime(@map_start_time),'dddd dd','es'))
||' de '||
format_date(to_date(@map_start_time),'MMMM','es')
|| ' de '||
format_date(to_datetime(@map_start_time),'yyyy','es')
||' a las'||
format_date(to_datetime(@map_start_time),' hh:mm')
||' horas'

#Unir puntos con líneas

make_line($geometry, geometry(get_feature('Ordered', 'ordena', "ordena"+1)) )

#Vecinos cercanos a la geometría

make_line(
    $geometry,(
    (
	overlay_nearest(
	'destinos',
	$geometry,
	limit:=-1,
	max_distance:=4040
	)
	)))

#Vecinos cercanos al cursor (el "fid" se puede cambiar por $geometry)

collect_geometries(array_foreach(string_to_array("fid"),
make_line(
    @canvas_cursor_point,(
    (
	overlay_nearest(
	'destinos',
	$geometry,
	limit:=3,
	max_distance:=5000
	)
	)))))

#contar objetos contenidos en otra capa directo de la calculadora de campos

aggregate('centros_de_salud', 'count', "fid", contains(geometry(@parent), $geometry))
